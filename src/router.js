import Vue from 'vue'
import VueRouter from 'vue-router'

import MainPage from './Main.vue'
import ToDoPage from './ToDo.vue'
import SearchPage from './Search.vue'
import ChatPage from './Chat.vue'
import ContactsPage from './Contacts.vue'

Vue.use(VueRouter);

export const router = new VueRouter({
   mode: 'history',
   routes: [
      { path: '/', component: MainPage },
      { path: '/todo', component: ToDoPage },
      { path: '/search', component: SearchPage },
      { path: '/chat', component: ChatPage },
      { path: '/contacts', component: ContactsPage }
   ]
})